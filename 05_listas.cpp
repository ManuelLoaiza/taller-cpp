#include <iostream>
using namespace std;

void SumarUno(int* ref_x) {
  (*ref_x)++;
}

void Modificar(int* ref_a, int* ref_b, int* ref_c) {
  (*ref_a) += 1;
  (*ref_b) += 2;
  (*ref_c) += 3;
}

int main(void) {
  int* arreglo;
  int n;
  cout << "Ingrese la cantidad de elementos: ";
  cin >> n;
  
  arreglo = (int*) malloc(sizeof(int) * n);

  for (int i = 0; i < n; i++) {
    *(arreglo + i) = i;
  }
  for (int i = 0; i < n; i++) {
    cout << *(arreglo + i) << endl;
  }

  free(arreglo);

  int x = 5;
  cout << x << endl;
  SumarUno(&x);
  cout << x << endl;

  int a, b, c;
  a = 2;
  b = 3;
  c = 10;
  Modificar(&a, &b, &c);
  cout << a << " " << b << " " << c << endl;
  return 0;
}
