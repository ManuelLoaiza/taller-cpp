#include <iostream>
#include <vector>
#include <map>
#include <set>

using namespace std;

void VerificarContacto(map<string, string> mapa, string nombre) {
  if (mapa.count(nombre) > 0) {
    cout << nombre << " es mi contacto" << endl;
  } else {
    cout << nombre << " no es mi contacto" << endl;
  }
}

int main(void) {
  vector<int> v;
  cout << v.size() << endl;
  
  v.push_back(10);
  cout<< v.size() << endl;

  v.push_back(11);
  cout << v.size() << endl;

  for (int i = 0; i < v.size(); i++) {
    cout << v[i] << endl;
  }

  v.pop_back();
  cout << v.size() << endl;

  map<string, string> contactos;
  contactos["rony"] = "+51 123 456 789";
  contactos["manuel"] = "+69 696 969 696";
  contactos["guillermo"] = "+12 357 111 317";

  cout << "El numero de rony es " << contactos["rony"] << endl;
  contactos["guillermo"] = "1234";
  cout << "El numero de guillermo es " << contactos["guillermo"] << endl;

  VerificarContacto(contactos, "rony");
  VerificarContacto(contactos, "yves");
  VerificarContacto(contactos, "manuel");
  VerificarContacto(contactos, "diego");

  if (!contactos.empty()) {
    cout << "Tengo contactos" << endl;
    cout << "Tengo " << contactos.size() << endl;
  }

  std::map<string, string>::iterator it;
  for (it = contactos.begin(); it != contactos.end(); it++) {
    cout << it->first << " : " << it->second << endl;
  }

  pair<int, int> punto;
  punto.first = 6;
  punto.second = 9;
  cout << punto.first << " " << punto.second << endl;

  set<int> unico;
  unico.insert(1);
  unico.insert(2);
  unico.insert(3);
  unico.insert(1);
  cout << unico.size() << endl;

  if (unico.count(4)) {
    cout << "Aparece 4" << endl;
  }

  if (unico.count(2)) {
    cout << "Aparece 2" << endl;
  }

  int a[9] = {2, -1, 5, 6, 10, 15, 30, 42, 69};
  int s = 17;
  bool aparecio = false;
  /*
  for (int i = 0; i < 9; i++) {
    for (int j = i + 1; j < 9; j++) {
      if (a[i] + a[j] == s) {
        cout << a[i] << " + " << a[j] << " = " << s << endl;
        aparecio = true;
        break;
      }
    }
    if (aparecio) break;
  }
  */
  set<int> ward;
  for (int i = 0; i < 9; i++) ward.insert(a[i]);
  for (int i = 0; i < 9; i++) {
    int falta = s - a[i];
    if (falta == a[i]) continue;
    if (ward.count(falta)) {
      cout << a[i] << " + " << falta << " = " << s << endl;
      aparecio = true;
      break;
    }
  }
  if (!aparecio) cout << "No aparecio" << endl;
  return 0;
}
