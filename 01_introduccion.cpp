#include <iostream>
using namespace std;

int main() {
  cout << "Hello world!" << endl;
  bool ok = true;  // true = 1, false = 0
  ok = false;
  if (ok) {
    cout << "Todo bien" << endl;
  } else {
    cout << "F" << endl;
  }

  // 4 bytes = 32 bits
  // [-2^31, 2^31 - 1]
  long long n, m;
  cout << "Ingrese n: ";
  cin >> n;
  cout << "Ingrese m: ";
  cin >> m;
  long long producto = n * m;
  cout << "n * m = " << producto << endl;

  // +, -, *, /, %
  if (producto % 2 == 0) {
    cout << producto << " es par" << endl;
  } else {
    cout << producto << " es impar" << endl;
  }
  
  int a = 5;
  double b = 2;
  cout << a / b << endl;

  char c = 'C';
  string s = "Rony";
  cout << "Hola, " << s << endl;
  return 0;
}
