#include <iostream>
#include <string>
#include <vector>
#define debug(x) cout << #x << " = " << x << endl
using namespace std;

typedef long long Long;

const Long B = 29;
const Long MOD = 1e9 + 7;

vector<Long> pot;
vector<Long> pref;

// O(|texto|)
void CalcularHash(string& texto) {
  int len = texto.size();
  pot.push_back(1LL);
  for (int i = 1; i <= len; i++) {
    pot.push_back((pot[i - 1] * B) % MOD);
  }
  pref.push_back(texto[0] - 'a' + 1);
  for (int i = 1; i < len; i++) {
    pref.push_back(((pref[i - 1] * B) % MOD + (texto[i] - 'a' + 1)) % MOD);
  }
}

// O(|texto| + |patron|)
int main(void) {
  string texto, patron;
  cin >> texto >> patron;

  // Calculamos el las potencias de B que usaremos y el hash de cada
  // prefijo del texto
  // O(|texto|)
  CalcularHash(texto);

  // Calculamos el hash del patron
  // O(|patron|)
  Long p = (patron[0] - 'a' + 1);
  int tamano_texto = texto.size();
  int tamano_patron = patron.size();
  for (int i = 1; i < tamano_patron; i++) {
    p = ((p * B) % MOD + (patron[i] - 'a' + 1)) % MOD;
  }


  vector<int> posiciones;
  // O(|texto|)
  for (int i = 0; i <= tamano_texto - tamano_patron; i++) {
    Long actual = pref[i + tamano_patron - 1];
    //debug(i);
    //debug(actual);
    if (i > 0) {
      actual = (actual - (pref[i - 1] * pot[tamano_patron]) % MOD + MOD) % MOD;
    }
    if (p == actual) {
      posiciones.push_back(i);
    }
  }

  int ans = posiciones.size();
  cout << "El patron aparece " << ans << " veces." << endl;
  cout << "Posiciones: ";
  for (int pos : posiciones) cout << pos << " ";
  cout << endl;
  return 0;
}
