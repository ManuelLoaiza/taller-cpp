#include <iostream>
#include <string>
using namespace std;

struct Nodo {
  char valor;
  Nodo* siguiente;
};

struct Pila {
  Nodo* cima;

  Pila() {
    cima = nullptr;
  }

  bool EsVacia(void) {
    return (cima == nullptr);
  }

  void Apilar(char valor) {
    Nodo* ref_nuevo_nodo = new Nodo();
    //(*ref_nuevo_nodo).valor = valor;
    ref_nuevo_nodo->valor = valor;
    ref_nuevo_nodo->siguiente = cima;
    cima = ref_nuevo_nodo;
  }

  char Desapilar(void) {
    char valor = cima->valor;
    Nodo* eliminar = cima;
    cima = cima->siguiente;
    delete eliminar;
    return valor;
  }
};

int main(void) {
  string secuencia;
  cin >> secuencia;
  Pila pila;
  int tamano = secuencia.size();
  for (int i = 0; i < tamano; i++) {
    if (secuencia[i] == '(') {
      pila.Apilar('(');
    } else {
      if (pila.EsVacia()) {
        cout << "NO" << endl;
        return 0;
      } else {
        pila.Desapilar();
      }
    }
  }

  if (pila.EsVacia()) {
    cout << "YES" << endl;
  } else {
    cout << "NO" << endl;
  }
  return 0;
}
