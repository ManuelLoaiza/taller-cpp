#include <iostream>
using namespace std;

/*
 * struct NombreStruct {
 *  tipo1 atributo1;
 *  tipo2 atributo2;
 *  ...
 *  tipok atributo k;
 *
 *  metodo1
 *  metodo2
 *  ...
 *  metodop
 *  };
 */

// TODO: Constructor
struct Punto {
  int x, y;

  Punto() {
    cout << "Hola, acabo de nacer" << endl;
    x = 0;
    y = 0;
  }

  Punto(int _x, int _y) {
    //cout << "Asignando coordenadas" << endl;
    x = _x;
    y = _y;
  }

  void imprimir(void) {
    cout << "(" << x << ", " << y << ")" << endl;
  }
  /*
  Punto operator + (Punto otro) {
    return Punto(x + otro.x, y + otro.y);
  }

  Punto operator * (int k) {
    return Punto(k * x, k * y);
  }
  */
};

Punto operator + (Punto punto1, Punto punto2) {
  return Punto(punto1.x + punto2.x, punto1.y + punto2.y);
}

Punto operator * (int k, Punto p) {
  return Punto(k * p.x, k * p.y);
}

bool operator < (Punto punto1, Punto punto2) {
  if (punto1.x == punto2.x) {
    return punto1.y < punto2.y;
  }
  return punto1.x < punto2.x;
}

struct Vector {
  Punto inicio, fin;
  
  Vector() {}
  
  Vector(Punto a, Punto b) {
    inicio = a;
    fin = b;
  }

  Punto valor(void) {
    return Punto(fin.x - inicio.x, fin.y - inicio.y);
  }

  void imprimir(void) {
    valor().imprimir();
  }
};

int main(void) {
  int n;
  cin >> n;
  Punto puntos[n];
  for (int i = 0; i < n; i++) {
    //cin >> puntos[i].x >> puntos[i].y;
    int x, y;
    cin >> x >> y;
    puntos[i] = Punto(x, y);
  }
  for (int i = 0; i < n; i++) puntos[i].imprimir();

  Punto p1 = Punto(1, 1);
  Punto p2 = Punto(5, 3);
  Vector v = Vector(p1, p2);
  v.imprimir();
  
  Punto p = puntos[0] + puntos[1];
  p.imprimir();
  p = 10 * p;
  p.imprimir();
  return 0;
}
