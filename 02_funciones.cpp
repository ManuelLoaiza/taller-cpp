#include <cmath>
#include <iomanip>
#include <iostream>
using namespace std;

typedef long long Long;
typedef long double Double;

const Long INF = 1e18;
const Double PI = acos(-1);
const int ALPH = 26;

// Recursivo
// Complejidad en tiempo: O(2^n)
int fibonacci(int n) {
  if (n <= 1) {
    return 1;
  } else {
    return (fibonacci(n - 1) + fibonacci(n - 2));
  }
}

// Iterativo
// Complejidad en tiempo: O(n)
Long fibonacci_iterativo(int n) {
  if (n <= 1) return 1LL;
  Long f0 = 1LL;
  Long f1 = 1LL;
  Long f;
  int actual = 2;
  while (actual <= n) {
    f = f0 + f1;
    f0 = f1;
    f1 = f;
    actual++;
  }
  return f;
}

void Saludar(string nombre) {
  cout << "Hola, " << nombre << endl;
}

void SumarUno(int& n) {
  n++;
}

void Leer(int v[], int n) {
  cout << "Ingrese " << n << " numeros enteros:" << endl;
  for (int i = 0; i < n; i++) cin >> v[i];
}

void ImprimirArreglo(int v[], int n) {
  for (int i = 0; i < n; i++) {
    if (i > 0) cout << " ";
    cout << v[i];
  }
  cout << endl;
}

// Retorna la cadena con caracteres en minuscula
void Minuscula(string& s) {
  int longitud = s.size();
  for (int i = 0; i < longitud; i++) {
    // Estamos frente a una mayuscula
    if (65 <= s[i] && s[i] <= 90) {
      // s[i] = s[i] + 32;
      s[i] += 32;
    }
  }
}

void ImprimirFrecuencia(string s) {
  cout << "Frecuencias de caracteres en la palabra " << s << endl;
  Minuscula(s);
  int longitud = s.size();
  int frecuencias[ALPH];
  for (int i = 0; i < ALPH; i++) frecuencias[i] = 0;
  for (int posicion = 0; posicion < longitud; posicion++) {
    int indice = s[posicion] - 'a';
    frecuencias[indice]++;
  }
  for (int i = 0; i < ALPH; i++) {
    if (frecuencias[i] == 0) continue;
    cout << char('a' + i) << ": " << frecuencias[i] << endl;
  }
}

int main() {
  int n;
  cin >> n;
  //cout << fibonacci(n) << endl;
  //cout << fibonacci_iterativo(n) << endl;
  cout << fixed << setprecision(18) << PI << endl;

  // Yves
  // Alfredo
  string nombre1 = "Yves";
  string nombre2 = "Alfredo";
  Saludar(nombre1);
  Saludar(nombre2);
  Saludar("Rony");

  // 0 ... 4
  int arreglo[5];
  for (int i = 0; i < 5; i++) {
    arreglo[i] = i + 1;
  }

  int x = 68;
  SumarUno(x);
  cout << "x = " << x << endl;

  // Arreglo : Allocada memoria adyacente de un mismo tipo de dato y el arreglo
  // apunta al primer elemento en memoria
  int v1[n], v2[n];
  Leer(v1, n);
  Leer(v2, n);
  ImprimirArreglo(v1, n);
  ImprimirArreglo(v2, n);

  ImprimirFrecuencia(nombre1);
  ImprimirFrecuencia(nombre2);
  ImprimirFrecuencia("AnaBanana");
  return 0;
}
